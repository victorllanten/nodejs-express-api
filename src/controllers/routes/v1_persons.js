var router = require('express').Router();
const helpers = require("../../commons/helpers")

var personsService = require('../../services/persons');

router.get('/:person_id?', (req, res, next) => {

  return personsService.get(
    (success) => {
      return helpers.success(req, res, next, success, 200);
    },
    (error) => {
      return helpers.failure(req, res, next, error, 200);
    },
    req.params.person_id);

});

router.post('/', (req, res, next) => {
  return personsService.save(
    (success) => {
      return helpers.success(req, res, next, success, 200);
    },
    (error) => {
      return helpers.failure(req, res, next, error, 200);
    },
    req.body);

});

router.put('/:person_id', (req, res, next) => {
  return personsService.update(
    (success) => {
      return helpers.success(req, res, next, success, 200);
    },
    (error) => {
      return helpers.failure(req, res, next, error, 200);
    },
    req.params.person_id,
    req.body);
});

router.delete('/:person_id', (req, res, next) => {
  return personsService.delete(
    (success) => {
      return helpers.success(req, res, next, success, 200);
    },
    (error) => {
      return helpers.failure(req, res, next, error, 200);
    },
    req.params.person_id);
});

module.exports = router;
