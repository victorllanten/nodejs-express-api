const helpers = require("../commons/helpers")

module.exports = {
  controllers: (app) => {

    app.get('/', (req, res, next) => {
      return helpers.success(req, res, next, "hello-world!", 200);
    });

    app.use('/api/v1/persons', require('./routes/v1_persons'));

  }
}
