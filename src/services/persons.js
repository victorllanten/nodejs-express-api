

const { success } = require("../commons/helpers");
var models = require("../models");

module.exports = {

  get: function (callbackOK, callbackError, person_id) {

    var params = {
      type: models.sequelize.QueryTypes.SELECT,
      replacements: {}
    }

    q = 'Select * from persons'

    if (person_id != undefined) {
      q += ' where id=:id'
      params.replacements = { id: person_id }
    }

    models.sequelize.query(q, params)
      .then(function (data) {
        callbackOK(data)
      })
      .catch(function (error) {
        callbackError(error)
      })

  },

  save: function (callbackOK, callbackError, newObj) {

    models.Persons.create(newObj)
      .then(function (success) {
        callbackOK(success)
      })
      .catch(function (error) {
        callbackError(error)
      })

  },

  update: function (callbackOK, callbackError, obj_id, newData,) {

    models.Persons.findByPk(obj_id)
      .then(function (data) {
        if (data) {
          data.update(newData)

          callbackOK(data)
        } else {
          callbackError('sin datos')
        }

      })
      .catch(function (error) {
        callbackError(error)
      })


  },

  delete: function (callbackOK, callbackError, obj_id) {

    models.Persons.destroy({
      where: {
          id: obj_id
        }
      })
      .then(function (data) {
        if (data) {
          callbackOK('deleted')
        } else {
          callbackError('sin datos')
        }
      })
      .catch(function (error) {
        callbackError(error)
      })

  },


}
