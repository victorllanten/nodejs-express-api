
// Add this to the VERY top of the first file loaded in your app
// var config = require(__dirname + '/config/config')[env];

var env = process.env.NODE_ENV || 'development';

// Instance main libraries
var express = require('express');
var api = require('./controllers/api');
var bodyParser = require('body-parser');
var cors = require('cors');

// Application
var app = express();
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors());

// Initialize apiController
api.controllers(app)

var port = 3001

app.listen(port);

console.log(`API start on http://127.0.0.1:${port}`);

process.on('uncaughtException', err => {
  console.log(err);
  process.exit(1);
});
