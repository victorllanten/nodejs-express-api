
function _respond(res,next,data,http_code,status,status_bool){
  res.setHeader('content-type','application/json');
  res.writeHead(http_code);
  res.end(
    JSON.stringify({
      "success" : status_bool,
      "status" : status,
      "data" : data
    })
  );
  return next();
}

module.exports = {
  "success" : function (req,res,next,data){
      _respond(res,next,data,200,"success",true);
  },
  "failure" : function (req,res,next,data,http_code){
      var time = ((new Date).getTime()) - req.start_time;
      _respond(res,next,data,http_code,"failure",false);
  },
  'invalidApi' : function(req, res){
      var response = {
        'status': 'failure',
        'data': 'You must specify a valid API key'
      };
      var time = ((new Date).getTime()) - req.start_time;
      res.setHeader('content-type', 'application/json');
      res.writeHead(401);
      res.end(JSON.stringify(response));
      return;
  },
  'tooManyRrequests' : function(req, res , type){
      var response = {
          'status': 'failure',
          'data': 'Too many requests per '+type
      };
      var time = ((new Date).getTime()) - req.start_time;
      res.setHeader('content-type', 'application/json');
      res.writeHead(429);
      res.end(JSON.stringify(response));
      return;
  },
  'invalidUserApi' : function(req, res, err){
    var response = {
        'status': 'failure',
        'data': 'The access_token does not exist'
    };
    var completeResponse = {
      'status': 'failure',
      'data': 'The access_token does not exist',
      'err': err
    };
    let time = ((new Date).getTime()) - req.start_time;
    res.setHeader('content-type', 'application/json');
    res.writeHead(401);
    res.end(JSON.stringify(response));
    return;
  },
  'no_records' : function(req, res){
    var response = {
        'status': 'failure',
        'data': 'This operation has no records to deliver.'
    };
    let time = ((new Date).getTime()) - req.start_time;
    res.setHeader('content-type', 'application/json');
    res.writeHead(204);
    res.end(JSON.stringify(response));
    return;
  }
}
