#FROM node:10.24.0
#RUN apt-get install -y git
FROM node:lts-alpine3.14
RUN apk --no-cache add git

WORKDIR /usr/src/app
COPY ./src /usr/src/app
RUN npm install
EXPOSE 3001
CMD [ "npm", "run" ,"startprod" ]
